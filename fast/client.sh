#curl --sSL https://gitee.com/hiberbug/pi/raw/master/fast/client.sh | sh
docker stop fast
docker rm fast
docker rmi -f fast:1.0
cd /var/www/FastTunnel.Client
docker build -t fast:1.0 .
docker run --name fast -it -d --restart=always -v /var/www/FastTunnel.Client:/app --net=host fast:1.0
